#ifndef LOGIC_H
#define LOGIC_H

#include <QAbstractListModel>
#include <QFile>
#include "params.h"
#include "cell.h"

enum Roles {
    Type,
    Index
};

class Logic : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(bool animation READ getValue WRITE setValue NOTIFY valueChanged)
    Q_PROPERTY(int width MEMBER (m_params.width) CONSTANT)
    Q_PROPERTY(int height MEMBER (m_params.height) CONSTANT)
    Q_PROPERTY(int minScore MEMBER (m_params.minScore) CONSTANT)
    Q_PROPERTY(int moves MEMBER m_moves NOTIFY changeMovesNumer)
    Q_PROPERTY(int score MEMBER m_score NOTIFY changeScoreCount)
    Q_PROPERTY(int gameOver MEMBER m_gameOver NOTIFY gameOver)

public:
    explicit Logic(QObject *parent = nullptr);
    ~Logic();

    bool getValue();

    Q_INVOKABLE bool move(int fromIndex, int toIndex, bool swap);
    Q_INVOKABLE bool checkWholeField();
    bool newGame();

public slots:
    void setValue(bool animationStatus);

signals:
    void changeMovesNumer();
    void changeScoreCount();
    void valueChanged();
    void gameOver();

protected:
    int rowCount(const QModelIndex &) const override;
    QVariant data(const QModelIndex & modelIndex, int role) const override;
    QHash<int, QByteArray> roleNames() const override;

private:
    Params  m_params;
    Cell    m_cells;
    int     m_moves;
    int     m_score;
    bool    m_animationStatus;
    bool    m_gameOver;

    void addCells(int destination);
    void deleteCells();
    void makeMove(int from, int to);
};

#endif // LOGIC_H
