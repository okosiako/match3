#ifndef CELL_H
#define CELL_H

#include <QList>
#include <QVector>
#include <QSet>

struct matchCell {
    int type;
    int index;
};

class Cell
{
public:
    QList<matchCell>    m_gameField;
    QList<int>          m_delete;

    Cell();

    void createField(int height, int width, QVector<int> &types);
    void swapCell(int fromIndex, int toIndex);
    bool findMatches(int height, int width);
    int  matchCount();

private:
    void addIndexesToDelete(QSet<int> &indexToDelete, int step, int currIndex, int currType);
};

#endif // CELL_H
