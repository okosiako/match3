#ifndef PARAMS_H
#define PARAMS_H

#include <QVector>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

struct Params {
    QVector<int>        types;
    int                 width;
    int                 height;
    int                 elementScore;
    int                 minScore;
    int                 maxMoves;

    bool writeParameters(const QJsonObject &parameters);
};

#endif // PARAMS_H
