import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Particles 2.0
import QtQuick.Dialogs 1.2

ApplicationWindow {
    id: root

    property int footerHeight: 30
    property var images      : [
        {'imgPath' : "/images/bird.png"},
        {'imgPath' : "/images/blue_butterfly.png"},
        {'imgPath' : "/images/cat.png"},
        {'imgPath' : "/images/dog.png"},
        {'imgPath' : "/images/parrot.png"},
        {'imgPath' : "/images/turtle.png"},
        {'imgPath' : "/images/zebra.png"},
        {'imgPath' : "/images/monarch_butterfly.png"},
        {'imgPath' : "/images/orange_butterfly.png"},
        {'imgPath' : "/images/tiger_butterfly.png"},
        {'imgPath' : "/images/star.png"}
    ]
    property bool imageScale: false
    property int squareSize  : 60
    property bool turnOnFirework: logic.gameOver

    visible: true

    title  : qsTr("Match 3")

    width        :  logic.width * squareSize
    height       :  logic.height * squareSize + footerHeight
    maximumWidth :  logic.width * squareSize
    maximumHeight:  logic.height * squareSize + footerHeight

    onTurnOnFireworkChanged: {
        particleSystem.restart();
    }

    GridView {
        id: cellsGrid

        property int  prevCellIndex   : logic.width
        property int  currCellIndex   : logic.width
        property bool highlightVisible: false
        property bool cellChoosed     : false
        property bool swap            : true

        model:       logic
        anchors.fill: parent

        cellWidth:   squareSize
        cellHeight:  squareSize

        interactive: false

        header: cellsGridFooter

        highlight: Rectangle {
            id:     cellHighlight;

            width:  squareSize;
            height: squareSize;
            color:  "dodgerblue";
            visible: cellsGrid.highlightVisible
        }

        delegate: cellsGridDelegate


        Rectangle {
            id: gridBackground

            width:  parent.width
            height: parent.height
            z:      logic.gameOver ? 10 : -10
            gradient: Gradient {
                GradientStop { position: 0.0; color: "gold" }
                GradientStop { position: 1.0; color: "tomato" }
            }
        }

        Binding {
            target: logic
            property: "animation"
            value: moveCells.running || addCells.running
        }

        move: Transition {
            id: moveCells
            NumberAnimation { properties: "x,y"; duration: 500; easing.type: Easing.OutBounce; alwaysRunToEnd: true }
            onRunningChanged: {
                if (!moveCells.running) {
                    if (logic.checkWholeField()) {
                        cellsGrid.swap = false;
                    }
                    else if (cellsGrid.swap){
                        cellsGrid.swap = false;
                        logic.move(cellsGrid.currCellIndex, cellsGrid.prevCellIndex, !cellsGrid.swap);
                    }
                }
            }
        }

        moveDisplaced: Transition {
            NumberAnimation { properties: "x,y"; duration: 500; easing.type: Easing.OutBounce }
        }

        add: Transition {
            id: addCells
            NumberAnimation { property: "y"; from: -squareSize; duration: 500; easing.type: Easing.OutBounce}
            NumberAnimation { property: "opacity"; from: 0; duration: 250 }
        }

        remove: Transition {
            id: deleteCells
            ParallelAnimation {
                NumberAnimation { property: "opacity"; to: 0; duration: 1000; }
                NumberAnimation { property: "scale"; to: 2; duration: 1000; }
            }
        }
    }

    Component {
        id: cellsGridFooter

        Rectangle {
            height: footerHeight
            width: parent.width
            z: 2
            gradient: Gradient {
                GradientStop { position: 0.0; color: "lightsteelblue" }
                GradientStop { position: 1.0; color: "blue" }
            }
            Text {
                id: moves

                text: "Moves: " + logic.moves
                color: "white"
                anchors.verticalCenter: parent.verticalCenter
            }
            Text {
                id: score

                anchors.left: moves.right
                anchors.leftMargin: parent.width / 3
                anchors.verticalCenter: parent.verticalCenter

                text: "Score: " + logic.score
                color: "white"
            }
        }
    }

    Component {
        id: cellsGridDelegate

        ShadowRectangle {

            width:   squareSize
            height:  squareSize

            visible: true
            scale:   0.9
            color: type !== 10 ? "white" : "transparent"

            Image {
                id:     img

                width:  squareSize - 10
                height: squareSize - 10
                anchors.centerIn: parent

                source: images[type].imgPath
            }

            MouseArea {
                anchors.fill: parent

                onClicked: {
                    parent.GridView.view.currentIndex = index;
                    cellsGrid.highlightVisible = true;

                    if (cellsGrid.cellChoosed) {
                        cellsGrid.highlightVisible = false;
                        cellsGrid.currCellIndex = cellIndex;
                        cellsGrid.swap = true;
                        logic.move(cellsGrid.prevCellIndex,
                                   cellIndex,
                                   !cellsGrid.swap);

                    }
                    else if (!cellsGrid.cellChoosed) {
                        cellsGrid.prevCellIndex = cellIndex;
                    }
                    cellsGrid.cellChoosed = !cellsGrid.cellChoosed;
                }
            }
        }
    }

    MessageDialog {
        id: messageWon

        visible: logic.gameOver ? true : false;
        title: "Game Over"
        text: {
            var mes = "Your score is: " + logic.score + "\nThe min is: " + logic.minScore + "\n so: "
                if (logic.minScore <= logic.score) {
                    return mes + "You won";
                }
                else {
                    return mes + "You loose";
                }
        }

        //onExept button start newGame
    }

    ParticleSystem {
        id: particleSystem
        paused: true

    }

    ImageParticle {
        id: smokePainter
        system: particleSystem
        groups: ['smoke']
        source: "images/out.png"
        alpha: 0.3
        entryEffect: ImageParticle.None
    }

    ImageParticle {
        id: rocketPainter

        system: particleSystem
        groups: ['rocket']
        source: "images/out.png"
        entryEffect: ImageParticle.None
    }

    Emitter {
        id: rocketEmitter

        anchors.bottom: parent.bottom
        width: parent.width
        height: 40
        system: particleSystem
        group: 'rocket'
        emitRate: 2
        maximumEmitted: 4
        lifeSpan: 4800
        lifeSpanVariation: 400
        size: 16
        velocity: AngleDirection {
            angle: 270
            magnitude: root.height / 2
            magnitudeVariation: 10
        }
        acceleration: AngleDirection {
            angle: 90
            magnitude: (root.height / 2) / 3
        }
    }

    Friction {
        groups: ['rocket']
        anchors.top: parent.top
        width: root.width
        height: root.height * 2/3
        system: particleSystem
        threshold: 5
        factor: 0.9
    }
    Turbulence {
        groups: ['rocket']
        anchors.bottom: parent.bottom
        width: parent.width
        height: 160
        system: particleSystem
        strength: 25
    }

    ParticleGroup {
        name: 'explosion'
        system: particleSystem

        TrailEmitter {
            id: explosionEmitter
            anchors.fill: parent
            group: 'sparkle'
            follow: 'rocket'
            lifeSpan: 750
            emitRatePerParticle: 200
            size: 32
            velocity: AngleDirection {
                angle: -90
                angleVariation: 180
                magnitude: (root.height / 2) / 3
            }
        }
    }

    GroupGoal {
        id: rocketChanger
        anchors.top: parent.top
        width: parent.width
        height: root.height * 2/3
        system: particleSystem
        groups: ['rocket']
        goalState: 'explosion'
        jump: true
    }

    ImageParticle {
        id: sparklePainter
        system: particleSystem
        groups: ['sparkle']
        source: "images/out.png"
        color: 'red'
        colorVariation: 0.6
        alpha: 0.3
    }
}


