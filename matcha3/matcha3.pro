TEMPLATE = app

QT += qml quick core widgets testlib
CONFIG += c++11

SOURCES += main.cpp \
    sources/logic.cpp \
    sources/params.cpp \
    sources/cell.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# remove possible other optimization flags
QMAKE_CXXFLAGS_RELEASE -= -O
QMAKE_CXXFLAGS_RELEASE -= -O1
QMAKE_CXXFLAGS_RELEASE -= -O2

# add the desired -O3 if not present
QMAKE_CXXFLAGS_RELEASE *= -O0

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    headers/logic.h \
    headers/params.h \
    headers/cell.h

DISTFILES += \
    images/bird.png \
    images/blue_butterfly.png \
    images/cat.png \
    images/dog.png \
    images/monarch_butterfly.png \
    images/orange_butterfly.png \
    images/parrot.png \
    images/tiger_butterfly.png \
    images/turtle.png \
    images/zebra.png \
    main.qml \
    ShadowRectangle.qml \
    firework.qml
