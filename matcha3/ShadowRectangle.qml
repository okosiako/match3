import QtQuick 2.0

Item {
    property alias color : rectangle.color

    BorderImage {
        anchors.fill: rectangle
        anchors.leftMargin: -6
        anchors.topMargin: -6
        anchors.rightMargin: -8
        anchors.bottomMargin: -8
        border.left: 10
        border.top: 10
        border.right: 10
        border.bottom: 10
        source: "images/blue-shadow.png"
    }

    Rectangle { id: rectangle; anchors.fill: parent; radius: 5 }
}
