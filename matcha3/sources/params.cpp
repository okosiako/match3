#include "headers/params.h"
#include <QDebug>
bool Params::writeParameters(const QJsonObject &parameters)
{
    QJsonArray typeArray = parameters["types"].toArray();
    if (typeArray.isEmpty()) {
        typeArray << 0 << 1 << 2 << 3 << 4;
    }
    for (int typeIndex = 0; typeIndex < typeArray.size(); ++typeIndex) {
        short type = typeArray[typeIndex].toInt() % 10;
        types.push_back(type);
    }

    width        = parameters["width"].toInt() < 1 ? 10 : parameters["width"].toInt();
    height       = parameters["height"].toInt() < 1 ? 10 : parameters["height"].toInt();
    elementScore = parameters["elementScore"].toInt() < 1 ? 60 : parameters["elementScore"].toInt();
    minScore     = parameters["minScore"].toInt() < 1 ? 10000 : parameters["minScore"].toInt();
    maxMoves     = parameters["maxMoves"].toInt() < 1 ? 20 : parameters["maxMoves"].toInt();

    bool paramsNotOk = width < 1 || height < 1 ||
            elementScore < 1 || minScore < 1 ||
            maxMoves < 1 || types.size() < 2;

    if (paramsNotOk) {
        return false;
    }
    return true;
}
