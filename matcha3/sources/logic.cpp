#include "headers/logic.h"
#include <QFileDialog>
#include <QDebug>
#include <QSet>
#include <iostream>
#include <QTimer>

Logic::Logic(QObject *parent) :
    QAbstractListModel(parent)
{
   if (!newGame()) {
       qWarning("For some reason default params were used");
   }
}

Logic::~Logic()
{
}

bool Logic::newGame()
{
    const QString   fileName = QFileDialog::getOpenFileName(0,
                                                            tr("Load configurations"), "", tr("Game configurations (*.json)"), 0, QFileDialog::DontUseNativeDialog);
    if (fileName.isEmpty()) {
        qWarning("You didn't choose a file");
    }

    QFile           configFile (fileName);

    if (!configFile.open(QIODevice::ReadOnly)) {
        qWarning("Couldn't open file with parameters.");
    }

    QByteArray      data = configFile.readAll();
    QJsonObject     configData = QJsonDocument::fromJson(data).object();

    if (configData.empty()) {
        qWarning("Configuration file is empty");
    }
    if (m_params.writeParameters(configData)) {
        m_cells.createField(m_params.height, m_params.width, m_params.types);
        while (checkWholeField()) {
            deleteCells();
        }
    }
    m_score = 0;
    m_moves = 0;
    m_animationStatus = true;
    m_gameOver = false;
    return true;
}

bool Logic::move(int fromIndex, int toIndex, bool swap)
{
    bool isLegalMove = abs(fromIndex - toIndex) == m_params.width ||
            (abs(fromIndex - toIndex) == 1 &&
             fromIndex / m_params.width == toIndex / m_params.width);
    if (!isLegalMove) {
        return false;
    }

    makeMove(std::max(fromIndex, toIndex), std::min(fromIndex, toIndex));
    if (!swap) {
        ++m_moves;
        emit changeMovesNumer();
    }
    return true;
}

void Logic::makeMove(int from, int to)
{
    const int   verticalMove = from % m_params.width == to % m_params.width;

    m_cells.swapCell(from, to);
    beginMoveRows(QModelIndex(), from, from, QModelIndex(), to);
    endMoveRows();
    if (verticalMove) {
        beginMoveRows(QModelIndex(), to + 1, to + 1, QModelIndex(), from + 1);
        endMoveRows();
    }
}

bool Logic::getValue()
{
    return m_animationStatus;
}

void Logic::setValue(bool animationStatus)
{
    if (m_animationStatus != animationStatus) {
        m_animationStatus = animationStatus;
    }

    if (m_animationStatus == false) {
        bool containsMatch = checkWholeField();
        if (containsMatch) {
            deleteCells();
        }
        else if (!containsMatch) {
            if (m_moves == m_params.maxMoves) {
                m_gameOver = true;
                emit gameOver();
            }
        }
    }
}

int Logic::rowCount(const QModelIndex & ) const {
    return m_cells.m_gameField.size();
}

QVariant Logic::data(const QModelIndex & modelIndex, int role) const {
    if (!modelIndex.isValid()) {
        return QVariant();
    }

    int index = static_cast<int>(modelIndex.row());

    if (index >= m_cells.m_gameField.size() || index < 0) {
        return QVariant();
    }

    matchCell cell = m_cells.m_gameField[index];

    switch (role) {
    case Roles::Type      : return cell.type;
    case Roles::Index     : return cell.index;
    }
    return QVariant();
}

QHash<int, QByteArray> Logic::roleNames() const {
    QHash<int, QByteArray> names;
    names.insert(Roles::Type  , "type");
    names.insert(Roles::Index , "cellIndex");
    return names;
}

bool Logic::checkWholeField()
{
    return m_cells.findMatches(m_params.height, m_params.width);
}

void Logic::deleteCells()
{
    for (int i = 0; i < m_cells.matchCount(); ++i) {
        addCells(m_cells.m_delete[i]);
        int from = m_cells.m_delete[i];
        int moveTo = m_cells.m_delete[i] % m_params.width;

        for ( ; from != moveTo; from -= m_params.width) {
            makeMove(from, from - m_params.width);
        }
    }
    m_score += m_cells.matchCount() * m_params.elementScore;
    emit changeScoreCount();
}

void Logic::addCells(int destination)
{
    int newType = m_params.types[qrand() % m_params.types.size()];

    m_cells.m_gameField[destination].type = 10;
    emit dataChanged(createIndex(destination, 0),
                     createIndex(destination, 0));

    beginRemoveRows(QModelIndex(), destination, destination);
    m_cells.m_gameField.removeAt(destination);
    endRemoveRows();

    beginInsertRows(QModelIndex(), destination, destination);
    m_cells.m_gameField.insert(destination,
                               matchCell {newType, destination});
    endInsertRows();
}
