#include "headers/cell.h"
#include <QDebug>

Cell::Cell()
{
}

void Cell::createField(int height, int width, QVector<int> &types)
{
    qsrand(time(NULL));
    for (int x = 0; x < height; ++x) {
        for (int y = 0; y < width; ++y) {
            int type = types[qrand() % types.size()];
            m_gameField << matchCell {type, x * width + y};
        }
    }
}

void Cell::swapCell(int fromIndex, int toIndex)
{
    m_gameField[fromIndex].index = toIndex;
    m_gameField[toIndex].index = fromIndex;

    m_gameField.swap(fromIndex, toIndex);
}

bool Cell::findMatches(int height, int width)
{
    QSet<int> indexToDelete;

    for (int x = 0; x < height; ++x) {
        for (int y = 0; y < width; ++y) {

            const int currIndex = x * width + y;
            const int currType = m_gameField[currIndex].type;

            if (y < width - 2) {
                addIndexesToDelete(indexToDelete, 1, currIndex, currType);
            }
            if (x < height - 2) {
                addIndexesToDelete(indexToDelete, width, currIndex, currType);
            }
        }
    }

    if (!m_delete.isEmpty())
        m_delete.clear();
    m_delete = indexToDelete.toList();

    if (m_delete.size()) {
        std::sort(m_delete.begin(), m_delete.end());
        return true;
    }
    return false;
}

int  Cell::matchCount()
{
    return m_delete.size();
}

void Cell::addIndexesToDelete(QSet<int> &indexToDelete, int step, int currIndex, int currType)
{
    bool match = m_gameField[currIndex + step].type ==
            currType && m_gameField[currIndex + step * 2].type == currType;

    if (match) {
        indexToDelete << currIndex << currIndex + step << currIndex + step * 2;
    }
}
