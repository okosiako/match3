#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "headers/logic.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    Logic logic;
    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("logic", &logic);
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    return app.exec();
}
